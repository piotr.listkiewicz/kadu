<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>@default</name>
    <message>
        <source>FileDesc</source>
        <translation>FileDesc</translation>
    </message>
    <message>
        <source>File with description to synchronize</source>
        <translation>File con la descrizione da sincronizzare</translation>
    </message>
    <message>
        <source>Enter here a file, which will contain descriptions to refresh by module.</source>
        <translation>Inserisci qui il un file che dovrà contenere le descrizioni aggiornate dal modulo</translation>
    </message>
    <message>
        <source>If you choose status without description, module will set it automatically to similar but with description</source>
        <translation>Se scegli uno stato senza descrizione, il modulo sarà settato automaticamente ad uno simile ma con la descrizione</translation>
    </message>
    <message>
        <source>Allow other descriptions</source>
        <translation>Consenti altre descrizioni</translation>
    </message>
    <message>
        <source>Allows you to set some custom description manualy, until file contents does not change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Always add description to status</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
