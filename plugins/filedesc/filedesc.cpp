/*
 * %kadu copyright begin%
 * Copyright 2012, 2013 Bartosz Brachaczek (b.brachaczek@gmail.com)
 * Copyright 2011, 2013, 2014 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QTimer>

#include "accounts/account-manager.h"
#include "accounts/account.h"
#include "configuration/configuration.h"
#include "configuration/deprecated-configuration-api.h"
#include "core/application.h"
#include "misc/paths-provider.h"
#include "status/status-changer-manager.h"
#include "debug.h"

#include "filedesc.h"

#define MODULE_FILEDESC_VERSION 1.14

// Implementation of FileDescStatusChanger class

FileDescStatusChanger::FileDescStatusChanger(FileDescription *parent, QObject *parentObj) :
		StatusChanger(900, parentObj), Parent(parent)
{
}

FileDescStatusChanger::~FileDescStatusChanger()
{
}

void FileDescStatusChanger::changeStatus(StatusContainer *container, Status &status)
{
	Q_UNUSED(container)

	if (status.isDisconnected())
		return;

	if (status.description().isEmpty() && !Parent->forceDesc())
		return;

	if (!status.description().isEmpty() && Parent->allowOther())
		return;

	status.setDescription(Title);
}

void FileDescStatusChanger::setTitle(const QString &title)
{
	Title = title;
	emit statusChanged(0);
}

// Implementation of FileDescription class

FileDescription::FileDescription(QObject *parent) :
		QObject(parent)
{
	kdebugf();

	createDefaultConfiguration();

	Timer = new QTimer(this);
	Timer->setSingleShot(false);
	Timer->setInterval(500);
	connect(Timer, SIGNAL(timeout()), this, SLOT(checkTitle()));
	Timer->start();

	StatusChanger = new FileDescStatusChanger(this, this);
	configurationUpdated();

	StatusChangerManager::instance()->registerStatusChanger(StatusChanger);
}

FileDescription::~FileDescription()
{
	kdebugf();
	Timer->stop();

	StatusChangerManager::instance()->unregisterStatusChanger(StatusChanger);
}

void FileDescription::configurationUpdated()
{
	File = Application::instance()->configuration()->deprecatedApi()->readEntry("FileDesc", "file", Application::instance()->pathsProvider()->profilePath() + QLatin1String("description.txt"));
	ForceDesc = Application::instance()->configuration()->deprecatedApi()->readBoolEntry("FileDesc", "forceDescr", true);
	AllowOther = Application::instance()->configuration()->deprecatedApi()->readBoolEntry("FileDesc", "allowOther", true);

	checkTitle();
}

void FileDescription::checkTitle()
{
	QFile file(File);

	if (!file.exists())
		return;

	if (!file.open(QIODevice::ReadOnly))
		return;

	QString description;
	QTextStream stream(&file);
	if (!stream.atEnd())
		description = stream.readLine();
	file.close();

	StatusChanger->setTitle(description);
}

void FileDescription::createDefaultConfiguration()
{
	Application::instance()->configuration()->deprecatedApi()->addVariable("FileDesc", "file", Application::instance()->pathsProvider()->profilePath() + QLatin1String("description.txt"));
	Application::instance()->configuration()->deprecatedApi()->addVariable("FileDesc", "forceDescr", true);
	Application::instance()->configuration()->deprecatedApi()->addVariable("FileDesc", "allowOther", true);
}

#include "moc_filedesc.cpp"
