<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>MediaPlayer</source>
        <translation>MediaPlayer</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>MPRIS Player</source>
        <translation>Odtwarzacz zgodny ze standardem MPRIS</translation>
    </message>
</context>
<context>
    <name>MPRISPlayerConfigurationUiHandler</name>
    <message>
        <source>Select Player:</source>
        <translation>Wybierz odtwarzacz:</translation>
    </message>
    <message>
        <source>Add Player</source>
        <translation>Dodaj odtwarzacz</translation>
    </message>
    <message>
        <source>Edit Player</source>
        <translation>Edytuj odtwarzacz</translation>
    </message>
    <message>
        <source>Delete Player</source>
        <translation>Usuń odtwarzacz</translation>
    </message>
</context>
<context>
    <name>MPRISPlayerDialog</name>
    <message>
        <source>Add Player</source>
        <translation>Dodaj odtwarzacz</translation>
    </message>
    <message>
        <source>Edit Player</source>
        <translation>Edytuj odtwarzacz</translation>
    </message>
    <message>
        <source>Player:</source>
        <translation>Odtwarzacz:</translation>
    </message>
    <message>
        <source>Service:</source>
        <translation>Usługa:</translation>
    </message>
</context>
</TS>
