<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>MediaPlayer</source>
        <translation>Přehrávač</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <source>MPRIS Player</source>
        <translation>Přehrávač MPRIS</translation>
    </message>
</context>
<context>
    <name>MPRISPlayerConfigurationUiHandler</name>
    <message>
        <source>Select Player:</source>
        <translation>Vybrat přehrávač:</translation>
    </message>
    <message>
        <source>Add Player</source>
        <translation>Přidat přehrávač</translation>
    </message>
    <message>
        <source>Edit Player</source>
        <translation>Upravit přehrávač</translation>
    </message>
    <message>
        <source>Delete Player</source>
        <translation>Smazat přehrávač</translation>
    </message>
</context>
<context>
    <name>MPRISPlayerDialog</name>
    <message>
        <source>Add Player</source>
        <translation>Přidat přehrávač</translation>
    </message>
    <message>
        <source>Edit Player</source>
        <translation>Upravit přehrávač</translation>
    </message>
    <message>
        <source>Player:</source>
        <translation>Přehrávač:</translation>
    </message>
    <message>
        <source>Service:</source>
        <translation>Služba:</translation>
    </message>
</context>
</TS>
