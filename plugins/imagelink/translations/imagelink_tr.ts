<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Konuşma</translation>
    </message>
    <message>
        <source>Show YouTube movies in chat window</source>
        <translation>YouTube videolarını konuşma penceresinde göster</translation>
    </message>
    <message>
        <source>Show images in chat window</source>
        <translation>Resimleri konuşma penceresinde göster</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Mesajlar</translation>
    </message>
</context>
</TS>
