<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversación</translation>
    </message>
    <message>
        <source>Show YouTube movies in chat window</source>
        <translation>Mostrar videos de YouTube en la ventana de conversación</translation>
    </message>
    <message>
        <source>Show images in chat window</source>
        <translation>Mostrar imágenes en la ventana de conversación</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Mensajes</translation>
    </message>
</context>
</TS>
