<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Keep contact list size</source>
        <translation>Zachovat velikost seznamu spojení</translation>
    </message>
    <message>
        <source>Hide scroll bar</source>
        <translation>Skrýt posuvník</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation>Navíc</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Jednoduchý pohled</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <source>Hide window borders</source>
        <translation>Skrýt okraje okna</translation>
    </message>
</context>
<context>
    <name>SimpleView</name>
    <message>
        <source>Simple view</source>
        <translation>Jednoduchý pohled</translation>
    </message>
</context>
</TS>
