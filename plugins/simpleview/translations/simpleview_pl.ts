<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Keep contact list size</source>
        <translation>Zachowuj rozmiar listy kontaktów</translation>
    </message>
    <message>
        <source>Hide scroll bar</source>
        <translation>Ukryj pasek przewijania</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation>Dodatki</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Widok uproszczony</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <source>Hide window borders</source>
        <translation>Ukryj obramowanie okna</translation>
    </message>
</context>
<context>
    <name>SimpleView</name>
    <message>
        <source>Simple view</source>
        <translation>Widok uproszczony</translation>
    </message>
</context>
</TS>
