<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>Public anahtar gönderildi</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Public anahtar gönderilirken hata meydana geldi</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Şifrelemede hata meydana geldi</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation>Bu şifreleme eklentisi önerilmiyor ve Kadu gelecekteki sürümlerinde kaldırılacaktır. Diğer anlık mesajlaşma uygulaması ile uyumluluk için OTR Şifreleme eklentisi kullanın.

</translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>OTR Şifrelemesi zaten aktif ve kullanılıyor.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>OTR Şifrelemesini şimdi aktif etmek istiyor musunuz?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>OTR Aktif</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>OTR Aktif değil</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>Public anahtar gönderildi: %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Public anahtar gönderilirken hata: %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Şifrelenirken hata meydana geldi</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>libqca2 için QCA OSSL eklentisi mevcut değil!</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>%1 arkadaşın public anahtarı gönderdi.
Kaydedecek misiniz?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Atla</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Şifrele</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>Şifreleme Yok</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>%1 Şifreleme</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Şifreleme Anahtarları Oluştur</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Şifreleme</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Anahtarlar oluşturuldu</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Anahtar oluşturmada hata</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Anahtar oluşturulamaz. encryption_ng_simlite eklentisi yüklüyse kontrol edin</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>Anahtarlar zaten mevcut. Üzerine yazmak istediğinize emin misiniz?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Anahtarların üzerine yaz</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
</TS>
