<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>Chave pública enviada</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Erro no envio da chave pública</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Erro na encriptação</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>Encriptação OTR já está ativo e pronto para usar.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>Você deseja ativar encriptação OTR agora?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>Habilitar OTR</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>Não habilitar OTR</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>Chave pública enviada para: %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Erro ao enviar chave pública para: %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Erro durante encriptação</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>O plugin QCA OSSL para libqca2 não está presente!</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>%1 está enviando sua chave pública.
Você quer salvá-la?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Encriptar</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>Sem criptografia</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>%1 criptografia</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Gerar chaves de criptografia</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Chaves foram geradas</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Erro gerando chaves</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Não foi possível gerar chaves. Verifique se o plugin encryption_ng_simlite está carregado</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>Chave já existente. Deseja sobrepor?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Sobrepor chaves</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
</TS>
