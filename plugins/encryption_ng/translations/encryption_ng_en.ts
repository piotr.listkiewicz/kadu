<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>Public key has been sent</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Error during sending public key</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Encryption error has occured</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>OTR Encryption is already active and ready to use.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>Do you want to activate OTR Encryption now?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>Enable OTR</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>Do not enable OTR</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>Public key has been send to: %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Error sending public key to: %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Error occured during encryption</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>The QCA OSSL plugin for libqca2 is not present!</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>Buddy %1 is sending you his public key.
Do you want to save it?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignore</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Encrypt</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>No Encryption</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>%1 Encryption</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Generate Encryption Keys</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Keys have been generated</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Error generating keys</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>Keys already exist. Do you want to overwrite them?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Overwrite keys</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
</TS>
