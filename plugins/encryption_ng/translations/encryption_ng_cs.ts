<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>Veřejný klíč byl poslán</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Chyba při posílání veřejného klíče</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Vyskytla se chyba v šifrování</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation>Tento šifrovací přídavný modul je odmítnut a bude v budoucích verzích Kadu odstraněn. Použijte šifrovací modul OTR pro kompatibilitu s jinými programi pro okamžitou výměnu zpráv.

</translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>Šifrování OTR je již zapnuto a připraveno k použití.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>Chcete zapnout šifrování OTR nyní?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>Povolit OTR</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>Nepovolit OTR</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>Veřejný klíč byl poslán: %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Chyba při posílání veřejného klíče: %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Během šifrování se vyskytla chyba</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>Přídavný modul QCA OSSL pro libqca2 není přítomen!</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>Kamarád %1 vám posílá svůj veřejný klíč.
Chcete jej uložit?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Přehlížet</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Zašifrovat</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>Bez šifrování</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>%1 šifrování</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Vytvořit šifrovací klíče</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Šifrování</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Klíče byly vytvořeny</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Chyba při vytváření klíčů</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Nelze vytvořit klíče. Prověřte, zda je nahrán přídavný modul encryption_ng_simlite</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>Klíče již existují. Chcete je přepsat?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Přepsat klíče</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
</TS>
