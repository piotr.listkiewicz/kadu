<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Cryptage</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>La clef publique a été envoyée</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Erreur durant l&apos;envoi de la clef publique</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Une erreur de cryptage est survenue</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation>Ce module de cryptage est obsolète et sera supprimé dans les futures versions de kadu. Utiliser le module de cryptage OTR en compatibilité avec d&apos;autres applications de messagerie instantanée.

</translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>Le cryptage OTR est déjà actif et près à être utilisé.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>Voulez vous activer le cryptage OTR maintenant ?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cryptage</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>Activer OTR</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>Désactiver OTR</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Cryptage</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>La clef publique a été envoyée à : %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Erreur d&apos;envoi de la clef publique à : %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Une erreur s&apos;est produite durant le cryptage</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Cryptage</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>L&apos;extension QCA OSSL pour libqca2 est manquante !</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>Votre ami %1 vous a envoyé sa clef publique.
Voulez-vous l&apos;enregistrer ?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cryptage</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Crypter</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>Pas de Cryptage</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>%1 Cryptage</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Générer les clefs de cryptage</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cryptage</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Les clefs ont été générées</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Erreur de génération des clefs</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Impossible de générer la clé. Vérifiez si le module encryption_ng_simlite est chargé</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>La clé existe déjà. Voulez vous l&apos;écrasez?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Écraser clés</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
</TS>
