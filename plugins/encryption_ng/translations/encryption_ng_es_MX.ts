<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>La llave pública ha sido enviada</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Error de envío de clave pública</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Error de cifrado</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation>Este complemento está obsoleto y será eliminado en futuras versiones de Kadu. Use el complemento de cifrado OTR para compatibilidad con otras aplicaciones de mensajería instantánea.
</translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>El cifrado OTR ya está activo y listo para usarse.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>¿Quieres activar el cifrado OTR ahora?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>Habilitar OTR</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>No habilitar OTR</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>La clave pública ha sido enviada a: %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Error al enviar la clave pública a: %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Se produjo un error durante el cifrado</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>¡El complemento QCA OSSL para libqca2 no está presente!</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>El compañero %1 le está enviando su clave pública.
¿Quiere guardarla?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Cifrar</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>Sin Cifrado</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>%1 Cifrado</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Generar Claves de  Cifrado</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Las claves han sido generadas</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Error en la generación de claves</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>No se pueden generar las claves. Compruebe si el complemento encryption_ng_simlite está cargado</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>Las claves ya existen. ¿Quiere sobrescribirlas?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Sobrescribir las claves</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
</TS>
