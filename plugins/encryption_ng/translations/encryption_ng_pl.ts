<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Public key has been sent</source>
        <translation>Klucz publiczny został wysłany</translation>
    </message>
    <message>
        <source>Error during sending public key</source>
        <translation>Błąd podczas wysyłania klucza publicznego</translation>
    </message>
    <message>
        <source>Encryption error has occured</source>
        <translation>Błąd szyfrowania</translation>
    </message>
</context>
<context>
    <name>EncryptionDepreceatedMessage</name>
    <message>
        <source>This encryption plugin is depreceated and will be removed in future versions of Kadu. Use OTR Encryption plugin for compatibility with other instant messenger application.

</source>
        <translation>Ta wtyczka szyfrowania jest przestarzała i zostanie usunięta w następnych wersjach Kadu. Użyj wtyczki szyfrowania OTR dla uzyskania kompatybilności z innymi komunikatorami.

</translation>
    </message>
    <message>
        <source>OTR Encryption is already active and ready to use.</source>
        <translation>Wtyczka szyfrowania OTW jest aktywna i gotowa do użycia.</translation>
    </message>
    <message>
        <source>Do you want to activate OTR Encryption now?</source>
        <translation>Aktywować szyfrowanie OTR?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Enable OTR</source>
        <translation>Włącz szyfrowanie OTW</translation>
    </message>
    <message>
        <source>Do not enable OTR</source>
        <translation>Nie włączaj szyfrowania OTR</translation>
    </message>
</context>
<context>
    <name>EncryptionNgNotification</name>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Public key has been send to: %1 (%2)</source>
        <translation>Klucz publiczny został wysłany do: %1 (%2)</translation>
    </message>
    <message>
        <source>Error sending public key to: %1 (%2)</source>
        <translation>Wystąpił błąd podczas wysyłania klucza do:  %1 (%2)</translation>
    </message>
    <message>
        <source>Error occured during encryption</source>
        <translation>Wystąpił błąd podczas szyfrowania</translation>
    </message>
</context>
<context>
    <name>EncryptionNgPlugin</name>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>The QCA OSSL plugin for libqca2 is not present!</source>
        <translation>Wtyczka QCA OSSL dla libqca2 jest niedostępna!</translation>
    </message>
</context>
<context>
    <name>EncryptionProviderManager</name>
    <message>
        <source>Buddy %1 is sending you his public key.
Do you want to save it?</source>
        <translation>Znajomy %1 przysłał Ci swój klucz publiczny.
Chcesz go zapisać?</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignoruj</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpActionDescription</name>
    <message>
        <source>Encrypt</source>
        <translation>Szyfruj</translation>
    </message>
</context>
<context>
    <name>EncryptionSetUpMenu</name>
    <message>
        <source>No Encryption</source>
        <translation>Brak szyfrowania</translation>
    </message>
    <message>
        <source>%1 Encryption</source>
        <translation>Szyfrowanie %1</translation>
    </message>
</context>
<context>
    <name>GenerateKeysActionDescription</name>
    <message>
        <source>Generate Encryption Keys</source>
        <translation>Wygeneruj klucze szyfrowania</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Keys have been generated</source>
        <translation>Klucze zostały wygenerowane</translation>
    </message>
    <message>
        <source>Error generating keys</source>
        <translation>Błąd generowania kluczy</translation>
    </message>
    <message>
        <source>Cannot generate keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Nie można wygenerować kluczy. Sprawdź czy wtyczka encryption_simlite jest załadowana</translation>
    </message>
    <message>
        <source>Keys already exist. Do you want to overwrite them?</source>
        <translation>Klucze istnieją. Czy chcesz je nadpisać?</translation>
    </message>
    <message>
        <source>Overwrite keys</source>
        <translation>Nadpisz klucze</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
</TS>
