<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Meşgul iken oto-gizleme süresi</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n saniye</numerusform>
        </translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation>Arkadaşlar listesi</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation>Arkadaşlar penceresi</translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation>Kullanılmadığında arkadaşlar listesini gizle</translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Gizleme</translation>
    </message>
</context>
</TS>
