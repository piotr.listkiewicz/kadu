<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>Fijar palabras</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>Habilitar fijación de palabra</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>Una palabra que se sustituye</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>Valor a sustituirse por</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>Palabra</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>Reemplazar por</translation>
    </message>
</context>
</TS>
