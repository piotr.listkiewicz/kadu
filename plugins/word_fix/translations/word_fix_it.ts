<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>Correzione parole</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>Abilita correzione parole</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation>Sillabazione</translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>Una parola da sostituire</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>Valore da sostituire con</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>Parola</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>Sotituisci con</translation>
    </message>
</context>
</TS>
