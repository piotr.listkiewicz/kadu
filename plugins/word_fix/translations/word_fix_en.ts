<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Words fix</source>
        <translation>Words fix</translation>
    </message>
    <message>
        <source>Enable word fix</source>
        <translation>Enable word fix</translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation>Spelling</translation>
    </message>
</context>
<context>
    <name>WordFix</name>
    <message>
        <source>A word to be replaced</source>
        <translation>A word to be replaced</translation>
    </message>
    <message>
        <source>Value to replace with</source>
        <translation>Value to replace with</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Change</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>Word</translation>
    </message>
    <message>
        <source>Replace with</source>
        <translation>Replace with</translation>
    </message>
</context>
</TS>
