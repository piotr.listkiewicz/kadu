project (screenshot)

cmake_minimum_required (VERSION 2.8)

find_package (Kadu REQUIRED CONFIG)

set (SOURCES
	configuration/gui/screenshot-configuration-ui-handler.cpp
	configuration/screen-shot-configuration.cpp
	gui/actions/screenshot-action.cpp
	gui/actions/screenshot-actions.cpp
	gui/graphics-items/handler-rect-item.cpp
	gui/graphics-items/selection-frame-item.cpp
	gui/widgets/crop-image-widget.cpp
	gui/widgets/screenshot-tool-box.cpp
	gui/widgets/screenshot-widget.cpp
	notification/screenshot-notification.cpp
	pixmap-grabber.cpp
	screenshot.cpp
	screenshot-plugin.cpp
	screen-shot-saver.cpp
	screenshot-taker.cpp
)

set (MOC_SOURCES
	configuration/gui/screenshot-configuration-ui-handler.h
	gui/actions/screenshot-action.h
	gui/actions/screenshot-actions.h
	gui/graphics-items/handler-rect-item.h
	gui/widgets/crop-image-widget.h
	gui/widgets/screenshot-tool-box.h
	gui/widgets/screenshot-widget.h
	notification/screenshot-notification.h
	screenshot.h
	screenshot-plugin.h
	screen-shot-saver.h
	screenshot-taker.h
)

set (CONFIGURATION_FILES
	data/configuration/screenshot.ui
)

if (UNIX AND NOT APPLE)
	set (PLUGIN_ADDITIONAL_QT_MODULES X11Extras)
	find_package (X11 REQUIRED COMPONENTS X11 Xext Xshape)
	if (X11_Xshape_FOUND)
		message (STATUS "Found required X11 libraries: ${X11_X11_LIB};${X11_Xext_LIB}")
	else ()
		message (SEND_ERROR "Could NOT find X11 Xshape")
	endif ()

	include_directories (${X11_INCLUDE_DIR})
	list (APPEND LIBRARIES ${X11_X11_LIB} ${X11_Xext_LIB})
endif ()

if (WIN32)
	set (PLUGIN_ADDITIONAL_QT_MODULES WinExtras)
endif ()

kadu_plugin (screenshot
	PLUGIN_SOURCES ${SOURCES}
	PLUGIN_MOC_SOURCES ${MOC_SOURCES}
	PLUGIN_CONFIGURATION_FILES ${CONFIGURATION_FILES}
	PLUGIN_LIBRARIES ${LIBRARIES}
	PLUGIN_ADDITIONAL_QT_MODULES ${PLUGIN_ADDITIONAL_QT_MODULES}
)
