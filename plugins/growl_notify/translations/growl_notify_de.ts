<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>%&amp;t - Titel (z.B neue Nachricht) %&amp;m - Inhalt der Benachrichtigung (z.B. Nachricht von neo), %&amp;d - Details (z.B. Nachrichteninhalt),%&amp;i - Smiley</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntax</translation>
    </message>
</context>
</TS>
