<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Growl no está instalado en su sistema</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - título (ejemplo. Nuevo mensaje) %&amp;m - texto de la notificación (ejemplo. Mensaje de Jim), %&amp;d - detalles (ejemplo. cita del mensaje),
%&amp;i - icono de la notificación</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sintaxis</translation>
    </message>
</context>
</TS>
