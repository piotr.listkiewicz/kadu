<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Growl n&apos;est pas installé sur votre système</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - titre (p. ex. Nouveau message) %&amp;m - notification texte (p. ex. Message de Jim), %&amp;d - détails (p. ex. message citation),
%&amp;i - notification icône</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntaxe</translation>
    </message>
</context>
</TS>
