<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Sisteminizde Growl yüklü değil</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - başlık (örn. Yeni mesaj) %&amp;m - uyarı yazısı (örn. Ahmet&apos;ten mesaj), %&amp;d - detaylar (örn. mesaj alıntısı),
%&amp;i - uyarı simgesi</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sözdizimi</translation>
    </message>
</context>
</TS>
