<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Growl não está instalado no seu sistema</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - título (ex. Nova mensagem) %&amp;m - texto de notificação (ex. Mensagem de Carlos), %&amp;d - detalhes (ex. citação de mensagem),
%&amp;i - ícone de notificação</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Sintaxe</translation>
    </message>
</context>
</TS>
