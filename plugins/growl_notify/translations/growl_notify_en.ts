<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Growl is not installed in your system</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntax</translation>
    </message>
</context>
</TS>
