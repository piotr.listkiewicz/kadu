<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>GrowlNotify</name>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Growl is not installed in your system</source>
        <translation>Growl nie jest zainstalowany</translation>
    </message>
</context>
<context>
    <name>GrowlNotifyConfigurationWidget</name>
    <message>
        <source>
%&amp;t - title (eg. New message) %&amp;m - notification text (eg. Message from Jim), %&amp;d - details (eg. message quotation),
%&amp;i - notification icon</source>
        <translation>%&amp;t - tytuł (np. Nowa wiadomość) %&amp;m - treść powiadomienia (np. Wiadomość od Jasia), %&amp;d - szczegóły (np. treść wiadomości),%&amp;i - ikona powiadomienia</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Tytuł</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Treść</translation>
    </message>
</context>
</TS>
