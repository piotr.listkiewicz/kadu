<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Son</translation>
    </message>
    <message>
        <source>Sound player</source>
        <translation>Lecteur audio</translation>
    </message>
    <message>
        <source>Player</source>
        <translation>Lecteur</translation>
    </message>
</context>
</TS>
