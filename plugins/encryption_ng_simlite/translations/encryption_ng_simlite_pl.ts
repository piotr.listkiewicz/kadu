<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation>Włącz szyfrowanie po otrzymaniu zaszyfrowanej wiadomości</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation>Nie można użyć klucza publicznego: niepoprawny klucz RSA</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation>Nie można użyć klucza publicznego: niepoprawne kodowanie BASE64</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation>Nie można użyć klucza publicznego: niepoprawny certyfikat PKSC1</translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation>Nie można użyć klucza publicznego: ten klucz nie zezwala na szyfrowanie</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation>Szyfrowanie niemożliwe: brak poprawnego klucza publicznego</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation>Szyfrowanie niemożliwe: brak poprawnego klucza blowfisha</translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation>Szyfrowanie niemożliwe: nieznany błąd</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation>Simlite</translation>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation>Wyślij mój klucz publiczny (Simlite)</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Nie można wysłać kluczy. Sprawdź czy wtyczka encryption_simlite jest załadowana</translation>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation>Klucz publiczny nie istnieje. Czy chcesz wygenerować nowy?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Tak</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <source>Error generating key</source>
        <translation>Błąd podczas generowania klucza</translation>
    </message>
    <message>
        <source>No public key available</source>
        <translation>Brak klucza publicznego</translation>
    </message>
</context>
</TS>
