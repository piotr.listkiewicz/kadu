<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversación</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation>Habilitar cifrado despues de recibir mensaje cifrado</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation>No se puede usar la clave pública: no hay una clave RSA válida</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation>No se puede usar la clave pública: codificación BASE64 no válida</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation>No se puede usar la clave pública: certificado PKCS1 no válido</translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation>No se puede usar la clave pública: esta clave no permite el cifrado</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation>No se puede cifrar: clave pública válida no disponible</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation>No se puede cifrar: clave Blowfish válida no disponible</translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation>No se puede cifrar: error desconocido</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation>Simlite</translation>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation>Enviar Mi Clave Pública (Simlite)</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>No se pueden enviar las claves. Compruebe si el complemento encryption_ng_simlite está cargado</translation>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation>La clave pública no existe. ¿Quiere crear una nueva?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Error generating key</source>
        <translation>Error generando clave</translation>
    </message>
    <message>
        <source>No public key available</source>
        <translation>Sin clave pública disponible</translation>
    </message>
</context>
</TS>
