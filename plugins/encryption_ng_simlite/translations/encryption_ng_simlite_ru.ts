<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error generating key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No public key available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
