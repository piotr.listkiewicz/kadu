<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Enable encrytpion after receiving encrypted message</source>
        <translation>Enable encrytpion after receiving encrypted message</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteEncryptor</name>
    <message>
        <source>Cannot use public key: not a valid RSA key</source>
        <translation>Cannot use public key: not a valid RSA key</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid BASE64 encoding</source>
        <translation>Cannot use public key: invalid BASE64 encoding</translation>
    </message>
    <message>
        <source>Cannot use public key: invalid PKCS1 certificate</source>
        <translation>Cannot use public key: invalid PKCS1 certificate</translation>
    </message>
    <message>
        <source>Cannot use public key: this key does not allow encrypttion</source>
        <translation>Cannot use public key: this key does not allow encrypttion</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid public key not available</source>
        <translation>Cannot encrypt: valid public key not available</translation>
    </message>
    <message>
        <source>Cannot encrypt: valid blowfish key not available</source>
        <translation>Cannot encrypt: valid blowfish key not available</translation>
    </message>
    <message>
        <source>Cannot encrypt: unknown error</source>
        <translation>Cannot encrypt: unknown error</translation>
    </message>
</context>
<context>
    <name>EncryptioNgSimliteProvider</name>
    <message>
        <source>Simlite</source>
        <translation>Simlite</translation>
    </message>
</context>
<context>
    <name>SimliteSendPublicKeyActionDescription</name>
    <message>
        <source>Send My Public Key (Simlite)</source>
        <translation>Send My Public Key (Simlite)</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation>Encryption</translation>
    </message>
    <message>
        <source>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</source>
        <translation>Cannot send keys. Check if encryption_ng_simlite plugin is loaded</translation>
    </message>
    <message>
        <source>Public key dont exist. Do you want to create new one?</source>
        <translation>Public key dont exist. Do you want to create new one?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Error generating key</source>
        <translation>Error generating key</translation>
    </message>
    <message>
        <source>No public key available</source>
        <translation>No public key available</translation>
    </message>
</context>
</TS>
