<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>Rady</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Pohled</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Rozvržení</translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation>Přidat nové rady</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Automaticky</translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation>Na tomto místě</translation>
    </message>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation>A</translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Nejmenší šířka</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Největší šířka</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation>Náhled na umístění rady</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Časové omezení</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Barva písma</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Barva pozadí</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Levé tlačítko</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Nic</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation>Prostřední tlačítko</translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Pravé tlačítko</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation>&apos;Otevřít okno pro rozhovor&apos; pracuje se všemi událostmi</translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation>Ukázat v radě obsah zprávy</translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation>Počet znaků daných do uvozovek</translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation>Smazat čekající zprávu, když uživatel smaže radu</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Seznam kamarádů</translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation>Rada ke kamarádovi</translation>
    </message>
    <message>
        <source>Border color</source>
        <translation>Barva okraje</translation>
    </message>
    <message>
        <source>Border width</source>
        <translation>Šířka okraje</translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation>Průhlednost</translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation>Tlačítka myši</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Skladba</translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation>Velikost ikony</translation>
    </message>
    <message>
        <source>16px</source>
        <translation>16px</translation>
    </message>
    <message>
        <source>22px</source>
        <translation>22px</translation>
    </message>
    <message>
        <source>32px</source>
        <translation>32px</translation>
    </message>
    <message>
        <source>48px</source>
        <translation>48px</translation>
    </message>
    <message>
        <source>64px</source>
        <translation>64px</translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation>Nový rozhovor/zpráva</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Pokročilé</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>1 sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation>Velikost okraje</translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation>Roh rady</translation>
    </message>
    <message>
        <source>On Top</source>
        <translation>Nahoře</translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation>Dole</translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation>Nahoře vlevo</translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation>Nahoře vpravo</translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation>Dole vlevo</translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation>Dole vpravo</translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation>Otevřít okno pro rozhovor</translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation>Smazat radu</translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation>Smazat všechny rady</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Zde&lt;/b&gt; si můžete prohlédnout náhled</translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation>Ukázat tlačítka, jen když oznámení vyžaduje činnost uživatele</translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation>Velikost a umístění rad...</translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation>Rada k nastavení kamaráda</translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation>Obnovit náhled</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Skladba</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation>Rada k seznamu kamarádů: </translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation>Pokročilé nastavení rad</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Zde&lt;/b&gt; si můžete prohlédnout náhled</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation>Nastavení rad</translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Neskrývat</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Zde&lt;/b&gt; si můžete prohlédnout náhled</translation>
    </message>
</context>
</TS>
