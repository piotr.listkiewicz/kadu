<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>@default</name>
    <message>
        <source>PC Speaker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PCSpeakerConfigurationWidget</name>
    <message>
        <source>Put the played sounds separate by space, _ for pause, eg. D2 C1# G0</source>
        <translation>Schreiben Sie Tonen isolieren durch Sperrung, zb. D2 C1# H0#</translation>
    </message>
</context>
</TS>
