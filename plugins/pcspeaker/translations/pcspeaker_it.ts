<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>@default</name>
    <message>
        <source>PC Speaker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PCSpeakerConfigurationWidget</name>
    <message>
        <source>Put the played sounds separate by space, _ for pause, eg. D2 C1# G0</source>
        <translation>Scriva i toni separato con suoi lungo, per pausa scrivo _ . Per esempio: D2/1 C1#/4 _/2 H0#/3</translation>
    </message>
</context>
</TS>
