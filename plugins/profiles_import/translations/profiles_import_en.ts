<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <source>Import external Kadu 0.6.5 profile</source>
        <translation>Import external Kadu 0.6.5 profile</translation>
    </message>
    <message>
        <source>Select profile path</source>
        <translation>Select profile path</translation>
    </message>
    <message>
        <source>Select profile path:</source>
        <translation>Select profile path:</translation>
    </message>
    <message>
        <source>Select imported account identity:</source>
        <translation>Select imported account identity:</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Import history</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Identity not selected&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Import external profile...</translation>
    </message>
    <message>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</translation>
    </message>
    <message>
        <source>Profile successfully imported!</source>
        <translation>Profile successfully imported!</translation>
    </message>
    <message>
        <source>Unable to import profile: %1</source>
        <translation>Unable to import profile: %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Import history</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Import external profile...</translation>
    </message>
    <message>
        <source>Profile %1 successfully imported!</source>
        <translation>Profile %1 successfully imported!</translation>
    </message>
    <message>
        <source>Import profile...</source>
        <translation>Import profile...</translation>
    </message>
    <message>
        <source>Unable to import profile: %1: %2</source>
        <translation>Unable to import profile: %1: %2</translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <source>Unable to open profile file [%1].</source>
        <translation>Unable to open profile file [%1].</translation>
    </message>
    <message>
        <source>Account already exists.</source>
        <translation>Account already exists.</translation>
    </message>
    <message>
        <source>Imported account has no ID</source>
        <translation>Imported account has no ID</translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <source>Import profiles...</source>
        <translation>Import profiles...</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Import external profile...</translation>
    </message>
</context>
</TS>
