<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <source>Import external Kadu 0.6.5 profile</source>
        <translation>Importar perfil externo de Kadu 0.6.5</translation>
    </message>
    <message>
        <source>Select profile path</source>
        <translation>Seleccionar la ruta del perfil</translation>
    </message>
    <message>
        <source>Select profile path:</source>
        <translation>Selecionar la ruta del perfil:</translation>
    </message>
    <message>
        <source>Select imported account identity:</source>
        <translation>Seleccionar la identidad de la cuenta importada:</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Importar historial</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Identidad no seleccionada&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;El directorio seleccionado no contiene el archivo kadu.conf.xml&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importar perfil externo...</translation>
    </message>
    <message>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>Este directorio no es un directorio del perfil de Kadu.
El archivo kadu.conf.xml no se encuentra</translation>
    </message>
    <message>
        <source>Profile successfully imported!</source>
        <translation>¡Perfil importado con éxito!</translation>
    </message>
    <message>
        <source>Unable to import profile: %1</source>
        <translation>No se puede importar el perfil: %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;L actual versión de Kadu no soporta perfiles de usuario.&lt;br /&gt;En su lugar, multiples cuentas son soportadas en una instancia de Kadu.&lt;/p&gt;&lt;p&gt;Por favor seleccione los perfiles que le gustaría importar como&lt;br /&gt;cuenta en esta instancia de Kadu.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Importar historial</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importar perfil externo...</translation>
    </message>
    <message>
        <source>Profile %1 successfully imported!</source>
        <translation>¡Perfil %1 importado con éxito!</translation>
    </message>
    <message>
        <source>Import profile...</source>
        <translation>Importar perfil...</translation>
    </message>
    <message>
        <source>Unable to import profile: %1: %2</source>
        <translation>No se puede importar el perfil: %1: %2</translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <source>Unable to open profile file [%1].</source>
        <translation>No se puede abrir el archivo del perfil [%1].</translation>
    </message>
    <message>
        <source>Account already exists.</source>
        <translation>La cuenta ya existe.</translation>
    </message>
    <message>
        <source>Imported account has no ID</source>
        <translation>La cuenta importada no tiene ID</translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <source>Import profiles...</source>
        <translation>Importar perfiles...</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importar perfil externo...</translation>
    </message>
</context>
</TS>
