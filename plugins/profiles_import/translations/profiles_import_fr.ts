<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <source>Import external Kadu 0.6.5 profile</source>
        <translation>Importer un profil externe Kadu 0.6.5</translation>
    </message>
    <message>
        <source>Select profile path</source>
        <translation>Sélectionner le chemin du profil</translation>
    </message>
    <message>
        <source>Select profile path:</source>
        <translation>Sélectionner le chemin du profil :</translation>
    </message>
    <message>
        <source>Select imported account identity:</source>
        <translation>Sélectionner l&apos;identité de compte importée :</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Importer l&apos;historique</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Identité non sélectionnée&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;Le répertoire sélectionné ne contient pas de fichier kadu.conf.xml&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importer un profil externe...</translation>
    </message>
    <message>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>Ce répertoire n&apos;est pas un répertoire de profil Kadu.
Fichier kadu.conf.xml introuvable</translation>
    </message>
    <message>
        <source>Profile successfully imported!</source>
        <translation>Profil importé avec succès !</translation>
    </message>
    <message>
        <source>Unable to import profile: %1</source>
        <translation>Impossible d&apos;importer le profile : %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;La version actuelle de Kadu ne prend pas en charge les profils utilisateur.&lt;br /&gt;Au lieu de cela, des comptes multiples sont pris en charge dans une instance de kadu.&lt;/p&gt;&lt;p&gt;Veuillez sélectionner les profils que vous souhaitez importer&lt;br /&gt;en tant compte dans cette instance de Kadu.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Importer l&apos;historique</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importer un profil externe...</translation>
    </message>
    <message>
        <source>Profile %1 successfully imported!</source>
        <translation>Profil %1 importé avec succès !</translation>
    </message>
    <message>
        <source>Import profile...</source>
        <translation>Importer un profil...</translation>
    </message>
    <message>
        <source>Unable to import profile: %1: %2</source>
        <translation>Impossible d&apos;importer le profile : %1 : %2</translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <source>Unable to open profile file [%1].</source>
        <translation>Impossible d&apos;ouvrir le fichier de profile [%1].</translation>
    </message>
    <message>
        <source>Account already exists.</source>
        <translation>Le compte existe déjà.</translation>
    </message>
    <message>
        <source>Imported account has no ID</source>
        <translation>Le compte importé n&apos;a pas d&apos;identifiant</translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <source>Import profiles...</source>
        <translation>Importer des profils...</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importer un profil externe...</translation>
    </message>
</context>
</TS>
