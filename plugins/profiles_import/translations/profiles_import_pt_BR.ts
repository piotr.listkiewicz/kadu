<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>ImportProfileWindow</name>
    <message>
        <source>Import external Kadu 0.6.5 profile</source>
        <translation>Importar perfil externo do Kadu 0.6.5</translation>
    </message>
    <message>
        <source>Select profile path</source>
        <translation>Selecione o caminho do perfil:</translation>
    </message>
    <message>
        <source>Select profile path:</source>
        <translation>Selecione o caminho do perfil:</translation>
    </message>
    <message>
        <source>Select imported account identity:</source>
        <translation>Selecione a identidade da conta importada:</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Importar histórico</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <source>&lt;b&gt;Identity not selected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Identidade não selecionada&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Selected directory does not contain kadu.conf.xml file&lt;/b&gt;</source>
        <translation>&lt;b&gt;Diretório selecionado não contém um arquivo kadu.conf.xml file&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importar perfil externo...</translation>
    </message>
    <message>
        <source>This directory is not a Kadu profile directory.
File kadu.conf.xml not found</source>
        <translation>Este diretório não é um diretório de perfil do Kadu.
Arquivo kadu.conf.xml não encontrado</translation>
    </message>
    <message>
        <source>Profile successfully imported!</source>
        <translation>Perfil importado com sucesso!</translation>
    </message>
    <message>
        <source>Unable to import profile: %1</source>
        <translation>Não foi possível importar perfil: %1</translation>
    </message>
</context>
<context>
    <name>ImportProfilesWindow</name>
    <message>
        <source>&lt;p&gt;Current version of Kadu does not support user profiles.&lt;br /&gt;Instead, multiple account are supported in one instances of kadu.&lt;/p&gt;&lt;p&gt;Please select profiles that you would like to import as&lt;br /&gt;account into this instance of Kadu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Versão atual do Kadu não suporta perfis de usuário.&lt;br /&gt;Em vez disso, multiplas contas são suportadas em uma instância do Kadu.&lt;/p&gt;&lt;p&gt;Por favor selecione os perfis que voce gostaria de importar como&lt;br /&gt;conta nesta instância do Kadu.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <source>Import history</source>
        <translation>Importar histórico</translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importar perfil externo...</translation>
    </message>
    <message>
        <source>Profile %1 successfully imported!</source>
        <translation>Perfil %1 importado com sucesso!</translation>
    </message>
    <message>
        <source>Import profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to import profile: %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileImporter</name>
    <message>
        <source>Unable to open profile file [%1].</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Imported account has no ID</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfilesImportActions</name>
    <message>
        <source>Import profiles...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import external profile...</source>
        <translation>Importar perfil externo...</translation>
    </message>
</context>
</TS>
