<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>Infos</name>
    <message>
        <source>&amp;Show infos about buddies</source>
        <translation>&amp;Arkadaşlar hakkında bilgiler göster</translation>
    </message>
</context>
<context>
    <name>InfosDialog</name>
    <message>
        <source>Buddies Information</source>
        <translation>Arkadaşların Bilgileri</translation>
    </message>
    <message>
        <source>Buddy</source>
        <translation>Arkadaş</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation>Protokol</translation>
    </message>
    <message>
        <source>Nick</source>
        <translation>Takma İsim</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Tanımlama</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Ülke</translation>
    </message>
    <message>
        <source>Last time seen on</source>
        <translation>En son görüldüğü zaman</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Kapat</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
</context>
</TS>
