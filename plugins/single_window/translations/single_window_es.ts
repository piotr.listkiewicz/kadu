<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <source>SingleWindow</source>
        <translation>VentanaÚnica</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Roster position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose position of roster in Single Window mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Atajos</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Cambiar a la pestaña anterior</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Cambiar a la pestaña siguiente</translation>
    </message>
    <message>
        <source>Show/hide roster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch focus between roster and tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation>Comportamiento</translation>
    </message>
</context>
</TS>
