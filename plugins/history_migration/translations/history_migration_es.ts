<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>El historial está siendo importado a un nuevo formato. Por favor, espera hasta que esta tarea haya terminado.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Progreso de los mensajes</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Importar historial...</translation>
    </message>
</context>
</TS>
