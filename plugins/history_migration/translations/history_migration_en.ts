<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>History is now being imported into new format. Please wait until this task is finished.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Chats progress:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Messages progress:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Import history...</translation>
    </message>
</context>
</TS>
