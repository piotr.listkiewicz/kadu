<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>El historial ahora está siendo importado a nuevo formato. Por favor espere a que se termine esta tarea.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Progreso de conversaciones:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Progreso de mensajes:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Importar historial...</translation>
    </message>
</context>
</TS>
