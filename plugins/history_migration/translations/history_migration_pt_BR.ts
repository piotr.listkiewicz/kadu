<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>HistoryImportWindow</name>
    <message>
        <source>History is now being imported into new format. Please wait until this task is finished.</source>
        <translation>O histórico agora está sendo importado para um novo formato. Por favor, espere até que o procedimento esteja concluído.</translation>
    </message>
    <message>
        <source>Chats progress:</source>
        <translation>Progresso de conversas:</translation>
    </message>
    <message>
        <source>Messages progress:</source>
        <translation>Progresso de mensagens:</translation>
    </message>
</context>
<context>
    <name>HistoryMigrationActions</name>
    <message>
        <source>Import history...</source>
        <translation>Importar histórico...</translation>
    </message>
</context>
</TS>
