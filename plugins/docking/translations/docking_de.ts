<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Systemabschnitt der Kontrolleiste</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Minimiert starten</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Stiller Modus</translation>
    </message>
</context>
</TS>
