<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Bandeja</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation>Envelope piscando</translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation>Envelope estático</translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation>Envelope animado</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Inicialização</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Iniciar minimizado</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation>Mostrar dicas no ícone da bandeja</translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation>Ícone da bandeja indicando nova mensagem</translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Recuperar</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Minimizar</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>&amp;Sair do Kadu</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Descrições</translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Modo silencioso</translation>
    </message>
</context>
</TS>
